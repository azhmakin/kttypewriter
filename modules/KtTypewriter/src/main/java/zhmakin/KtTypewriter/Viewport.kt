package zhmakin.KtTypewriter

import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import javax.swing.JPanel


/**
 * Renders a virtual page of paper to type on.
 * @author Andrey Zhmakin
 */
class Viewport(private val typewriter: KtTypewriter) : JPanel(), KeyListener
{
    var page : BufferedImage? = null

    private val offsetX : Int = 0
    private val offsetY : Int = 0

    private var cursorX : Int = 50
    private var cursorY : Int = 50

    private var altPressed : Boolean = false
    private var shiftPressed : Boolean = false
    private var ctrlPressed : Boolean = false

    init {
        this.addKeyListener(this)
        this.isFocusable = true
        this.grabFocus()
        this.requestFocus()
    }


    override fun paintComponent(g: Graphics?)
    {
        //super.paintComponent(g)

        if (page == null)
        {
            return
        }

        val g2 = g as Graphics2D

        g2.drawImage(page, offsetX, offsetY, null)

        g2.color = Color.RED
        g2.drawLine( offsetX + cursorX, offsetY + cursorY + 12, offsetX + cursorX + 10, offsetY + cursorY + 12 )
    }


    override fun keyTyped(e: KeyEvent?)
    {

    }


    override fun keyPressed(e: KeyEvent?)
    {
        //println(e!!.keyCode)

        if (e!!.keyCode == KeyEvent.VK_ALT)
        {
            altPressed = true
            return
        }

        if (e!!.keyCode == KeyEvent.VK_SHIFT)
        {
            shiftPressed = true
            return
        }

        if (e!!.keyCode == KeyEvent.VK_CONTROL)
        {
            ctrlPressed = true
            return
        }

        when (e!!.keyCode)
        {
            //KeyEvent.VK_LEFT -> null;

            KeyEvent.VK_UP ->
                { cursorY -= 1
                  repaint()
                  return }

            //KeyEvent.VK_RIGHT -> null;

            KeyEvent.VK_DOWN ->
                { cursorY += 1
                  repaint()
                  return }
        }

        if (e!!.keyCode == 8)
        {
            cursorX -= 8//10 - 1
            repaint()
            return
        }

        if (e!!.keyCode == 10)
        {
            cursorX = 50
            cursorY += 14
            repaint()
            return
        }

        val glyph = when (e!!.keyChar)
            {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> loadGlyph(String.format("./fonts/%03d.png", 74 + e!!.keyCode - 48))

                ' '  -> loadGlyph("./fonts/084.png") // Space

                'F'  -> loadGlyph("./fonts/000.png") // А
                '<'  -> loadGlyph("./fonts/001.png") // Б
                'D'  -> loadGlyph("./fonts/002.png") // В
                'U'  -> loadGlyph("./fonts/003.png") // Г
                'L'  -> loadGlyph("./fonts/004.png") // Д
                'T'  -> loadGlyph("./fonts/005.png") // Е
                '~'  -> loadGlyph("./fonts/006.png") // Ё
                ':'  -> loadGlyph("./fonts/007.png") // Ж
                'P'  -> loadGlyph("./fonts/008.png") // З
                'B'  -> loadGlyph("./fonts/009.png") // И
                'Q'  -> loadGlyph("./fonts/010.png") // Й
             // ' '  -> loadGlyph("./fonts/011.png") // І
                'R'  -> loadGlyph("./fonts/012.png") // К
                'K'  -> loadGlyph("./fonts/013.png") // Л
                'V'  -> loadGlyph("./fonts/014.png") // М
                'Y'  -> loadGlyph("./fonts/015.png") // Н
                'J'  -> loadGlyph("./fonts/016.png") // О
                'G'  -> loadGlyph("./fonts/017.png") // П
                'H'  -> loadGlyph("./fonts/018.png") // Р
                'C'  -> loadGlyph("./fonts/019.png") // С
                'N'  -> loadGlyph("./fonts/020.png") // Т
                'E'  -> loadGlyph("./fonts/021.png") // У
                'A'  -> loadGlyph("./fonts/022.png") // Ф
                '{'  -> loadGlyph("./fonts/023.png") // Х
                'W'  -> loadGlyph("./fonts/024.png") // Ц
                'X'  -> loadGlyph("./fonts/025.png") // Ч
                'I'  -> loadGlyph("./fonts/026.png") // Ш
                'O'  -> loadGlyph("./fonts/027.png") // Щ
                '}'  -> loadGlyph("./fonts/028.png") // Ъ
                'S'  -> loadGlyph("./fonts/029.png") // Ы
                'M'  -> loadGlyph("./fonts/030.png") // Ь
             // ' '  -> loadGlyph("./fonts/031.png") // Ѣ
                '"'  -> loadGlyph("./fonts/032.png") // Э
                '>'  -> loadGlyph("./fonts/033.png") // Ю
                'Z'  -> loadGlyph("./fonts/034.png") // Я

                'f'  -> loadGlyph("./fonts/037.png") // а
                ','  -> loadGlyph("./fonts/038.png") // б
                'd'  -> loadGlyph("./fonts/039.png") // в
                'u'  -> loadGlyph("./fonts/040.png") // г
                'l'  -> loadGlyph("./fonts/041.png") // д
                't'  -> loadGlyph("./fonts/042.png") // е
                '`'  -> loadGlyph("./fonts/043.png") // ё
                ';'  -> loadGlyph("./fonts/044.png") // ж
                'p'  -> loadGlyph("./fonts/045.png") // з
                'b'  -> loadGlyph("./fonts/046.png") // и
                'q'  -> loadGlyph("./fonts/047.png") // й
             // ' '  -> loadGlyph("./fonts/048.png") // і
                'r'  -> loadGlyph("./fonts/049.png") // к
                'k'  -> loadGlyph("./fonts/050.png") // л
                'v'  -> loadGlyph("./fonts/051.png") // м
                'y'  -> loadGlyph("./fonts/052.png") // н
                'j'  -> loadGlyph("./fonts/053.png") // о
                'g'  -> loadGlyph("./fonts/054.png") // п
                'h'  -> loadGlyph("./fonts/055.png") // р
                'c'  -> loadGlyph("./fonts/056.png") // с
                'n'  -> loadGlyph("./fonts/057.png") // т
                'e'  -> loadGlyph("./fonts/058.png") // у
                'a'  -> loadGlyph("./fonts/059.png") // ф
                '['  -> loadGlyph("./fonts/060.png") // х
                'w'  -> loadGlyph("./fonts/061.png") // ц
                'x'  -> loadGlyph("./fonts/062.png") // ч
                'i'  -> loadGlyph("./fonts/063.png") // ш
                'o'  -> loadGlyph("./fonts/064.png") // щ
                ']'  -> loadGlyph("./fonts/065.png") // ъ
                's'  -> loadGlyph("./fonts/066.png") // ы
                'm'  -> loadGlyph("./fonts/067.png") // ь
             // ' '  -> loadGlyph("./fonts/068.png") // ѣ
                '\'' -> loadGlyph("./fonts/069.png") // э
                '.'  -> loadGlyph("./fonts/070.png") // ю
                'z'  -> loadGlyph("./fonts/071.png") // я

                '/'  -> loadGlyph("./fonts/085.png") // .
                '?'  -> loadGlyph("./fonts/086.png") // ,
                '$'  -> loadGlyph("./fonts/087.png") // ;
                '^'  -> loadGlyph("./fonts/088.png") // :
                '!'  -> loadGlyph("./fonts/089.png") // !
                '&'  -> loadGlyph("./fonts/090.png") // ?

                '+'  -> loadGlyph("./fonts/091.png") // +
                '-'  -> loadGlyph("./fonts/092.png") // -
                '*'  -> loadGlyph("./fonts/093.png") // *
             // '/'  -> loadGlyph("./fonts/094.png") // /
                '\\' -> loadGlyph("./fonts/095.png") // \
                '@'  -> loadGlyph("./fonts/096.png") // @
                '='  -> loadGlyph("./fonts/098.png") // =
             // '\'' -> loadGlyph("./fonts/099.png") // '
                '%'  -> loadGlyph("./fonts/100.png") // %
                '|'  -> loadGlyph("./fonts/104.png") // |
                '_'  -> loadGlyph("./fonts/107.png") // _
                '#'  -> loadGlyph("./fonts/109.png") // №
                '('  -> loadGlyph("./fonts/111.png") // (
                ')'  -> loadGlyph("./fonts/112.png") // )

                else -> null
            }

        if (glyph != null)
        {
            drawGlyph( page, glyph, cursorX + offsetX, cursorY + offsetY)

            cursorX += glyph.width

            this.repaint()
        }
    }


    override fun keyReleased(e: KeyEvent?)
    {
        if (e!!.keyCode == KeyEvent.VK_ALT)
        {
            altPressed = false
            return
        }

        if (e!!.keyCode == KeyEvent.VK_SHIFT)
        {
            shiftPressed = false
            return
        }

        if (e!!.keyCode == KeyEvent.VK_CONTROL)
        {
            ctrlPressed = false
            return
        }
    }


    private fun loadGlyph(filename: String) : BufferedImage
    {
        return ImageIO.read(File(filename))
    }


    private fun drawGlyph(page : BufferedImage?, glyph: BufferedImage, x:Int, y:Int)
    {
        if (page == null)
        {
            return
        }

        for (iy in 0 until glyph.height)
        {
            for (ix in 0 until glyph.width)
            {
                val rgb = glyph.getRGB(ix, iy)

                if ((rgb and 0xFF_FF_FF) != 0xFF_FF_FF)
                {
                    val r = (rgb shr 16) and 0xFF
                    val g = (rgb shr  8) and 0xFF
                    val b = (rgb       ) and 0xFF

                    val v = ((r + g + b) / 3) and 0xFF

                    glyph.setRGB(ix, iy, (typewriter.tapeColor.rgb and 0xFF_FF_FF) or ((255 - v) shl 24))
                }
                else
                {
                    glyph.setRGB(ix, iy, 0)
                }
            }
        }

        page.graphics.drawImage(glyph, x, y, null)
    }
}
