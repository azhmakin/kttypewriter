package zhmakin.KtTypewriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

public class DlgCreatePage extends JDialog
{
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox cmbUnits;
    private JTextField txtWidth;
    private JTextField txtHeight;
    private JLabel lblWidthUnits;
    private JLabel lblHeightUnits;
    private JTextField txtDPI;

    public BufferedImage image;


    public DlgCreatePage()
    {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        cmbUnits.addItemListener(e -> onUnitChange());

        onUnitChange();

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }


    private void onUnitChange()
    {
        if (cmbUnits.getSelectedItem().equals("Inch"))
        {
            this.lblWidthUnits.setText("\"");
            this.lblHeightUnits.setText("\"");
        }
        else
        {
            this.lblWidthUnits.setText("mm");
            this.lblHeightUnits.setText("mm");
        }
    }


    private void onOK()
    {
        double inWidth;
        double inHeight;

        if (cmbUnits.getSelectedItem().equals("Inch"))
        {
            inWidth  = Double.parseDouble( txtWidth.getText () );
            inHeight = Double.parseDouble( txtHeight.getText() );
        }
        else
        {
            inWidth  = Integer.parseInt( txtWidth.getText () ) / 25.4;
            inHeight = Integer.parseInt( txtHeight.getText() ) / 25.4;
        }

        int dpi = Integer.parseInt( txtDPI.getText() );

        int pxWidth  = (int) Math.round( dpi * inWidth  );
        int pxHeight = (int) Math.round( dpi * inHeight );

        this.image = new BufferedImage(pxWidth, pxHeight, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = (Graphics2D) this.image.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, image.getWidth(), image.getHeight());

        dispose();
    }


    private void onCancel()
    {
        dispose();
    }


    public static BufferedImage image(Window window)
    {
        DlgCreatePage dialog = new DlgCreatePage();
        dialog.pack();
        dialog.setLocationRelativeTo(window);
        dialog.setVisible(true);

        return dialog.image;
    }



    public static void main(String[] args)
    {
        DlgCreatePage dialog = new DlgCreatePage();
        dialog.pack();
        //dialog.setLocationRelativeTo();
        dialog.setVisible(true);
        System.exit(0);
    }
}
