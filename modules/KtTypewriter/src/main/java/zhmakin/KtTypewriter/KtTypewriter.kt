package zhmakin.KtTypewriter

import java.awt.BorderLayout
import java.awt.Color
import java.io.File
import javax.imageio.ImageIO
import javax.swing.*


/**
 * Main class; Main window and controls.
 * @author Andrey Zhmakin
 */
class KtTypewriter : JFrame("Typewriter")
{
    val viewport : Viewport = Viewport(this)
    val toolbar : JToolBar = JToolBar()
    var filename : String? = null
    var tapeColor : Color = Color.BLACK

    val cmdTapeColor = JButton("Tape")


    init
    {
        toolbar.isRollover = true
        toolbar.isFocusable = false

        val cmdNew = JButton("New")
        val cmdOpen = JButton("Open")
        val cmdSave = JButton("Save")
        val cmdSaveAs = JButton("Save As")


        cmdNew.isFocusable = false
        cmdOpen.isFocusable = false
        cmdSave.isFocusable = false
        cmdSaveAs.isFocusable = false
        cmdTapeColor.isFocusable = false

        cmdNew.addActionListener { newSheet() }
        cmdOpen.addActionListener { openSheet() }
        cmdSave.addActionListener { saveSheet() }
        cmdSaveAs.addActionListener { saveSheetAs() }
        cmdTapeColor.addActionListener { chooseTapeColor() }

        cmdTapeColor.background = tapeColor

        toolbar.add(cmdNew)
        toolbar.add(cmdOpen)
        toolbar.add(cmdSave)
        toolbar.add(cmdSaveAs)
        toolbar.add(cmdTapeColor)

        this.contentPane.add(viewport, BorderLayout.CENTER)
        this.contentPane.add(toolbar, BorderLayout.NORTH)

        this.setSize(500, 400)
        this.setLocationRelativeTo(null)

        // TODO: DO NOTHING ON CLOSE
        this.defaultCloseOperation = EXIT_ON_CLOSE

        this.isVisible = true
    }


    private fun newSheet()
    {
        val image = DlgCreatePage.image(this)

        if (image != null)
        {
            viewport.page = image
            viewport.repaint()
        }
    }


    private fun openSheet()
    {
        //TODO: Prompt saving the existing image!
        if (filename != null)
        {

        }

        val fc = JFileChooser()

        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            viewport.page = ImageIO.read(fc.selectedFile)
            filename = fc.selectedFile.absolutePath
            println(filename)
            viewport.repaint()
        }
    }


    private fun saveSheet()
    {
        if (viewport.page == null)
        {
            return
        }

        if (filename != null)
        {
            ImageIO.write(viewport.page, "png", File(filename))
        }
        else
        {
            saveSheetAs()
        }
    }


    private fun saveSheetAs()
    {
        if (viewport.page == null)
        {
            return
        }

        val fc = JFileChooser()

        if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            ImageIO.write(viewport.page, "png", fc.selectedFile)
            filename = fc.selectedFile.absolutePath
        }
    }


    private fun chooseTapeColor()
    {
        val newColor = JColorChooser.showDialog(this, "Choose Tape Color", tapeColor)

        if (newColor != null)
        {
            tapeColor = newColor
            cmdTapeColor.background = tapeColor
        }
    }
}


fun main(args: Array<String>)
{
    KtTypewriter()
}
